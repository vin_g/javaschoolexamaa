package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    // Vinnikov Grigory vinnikov@inbox.ru
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        // check if both of inputted expressions are null
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        // check both of inputted expressions possible or not
        Object elementFmExpressionX;
        Object elementFmExpressionY;
        int buf = 0;
        boolean possible = true;

        for (int i = 0; i < x.size(); i++) {
            if (possible) {
                possible = false;
                elementFmExpressionX = x.get(i);
                for (int j = buf; j < y.size(); j++) {
                    elementFmExpressionY = y.get(j);
//                    System.out.println("-------34-:" + i + "**"+ j + "**"+ possible);
//                    System.out.println("-------35-:" + elementFmExpressionX + "**"+elementFmExpressionY + "**"+ possible);
                    try {
                        if (elementFmExpressionX.equals(elementFmExpressionY)) {
                            buf = j + 1;
                            possible = true;
//                            System.out.println("-------40-:" + elementFmExpressionX + "**"+elementFmExpressionY + "**"+ possible);
                            break;
                        }
                    } catch (Exception e) {
                        if (elementFmExpressionX == elementFmExpressionY) {
                            buf = j;
                            possible = true;
//                  System.out.println("-------47-:" + elementFmExpressionX + "**"+elementFmExpressionY + "**"+ possible);
                            break;
                        }
                    }
                }
            }
            if (!possible) {
                break;
            }
        }
        return possible;
    }
}
