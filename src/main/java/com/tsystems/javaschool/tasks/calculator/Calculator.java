package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.service.MathsDoings;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Calculator implements MathsDoings { // калькулятор

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
// Vinnikov Grigory vinnikov@inbox.ru
    private final String SUM = "+";
    private final String MINUS = "-";
    private final String MULTI = "*";
    private final String DIVID = "/";
    private final String resultNull = null;
    private String inputedStatement = "";
    private ArrayList<String> listFmInputedStatement;

    public String evaluate(String statement) {
        // TODO: Implement the logic here
//        System.out.println("-------- evaluate begins");
        // begin to check and return null if smthng wrong on the first step
        if(statement == null || statement.length() < 3 || statement.isEmpty())
        {
//            System.out.println("****** 27 statement->" + statement);
            return resultNull;
        } else
        if(statement.trim().replaceAll(" ", "").contains("/0")
                && statement.trim().replaceAll(" ", "").length() == 3)
        {
//            System.out.println("****** 33 statement->" + statement);
            return resultNull;
        } else
        {
            if (!statement.contains(SUM) && !statement.contains(MINUS) && !statement.contains(MULTI)
                    && !statement.contains(DIVID))
            {
//                System.out.println("****** 39 statement->" + statement);
                return resultNull;
            } else
            {
                String statementWithoutWhitespace = statement.trim().replaceAll(" ","");
                String checkIfText = statementWithoutWhitespace.replaceAll("[0-9]", "")
                        .replaceAll("\\+","").replaceAll(",","")
                        .replaceAll(MINUS,"").replaceAll("\\*","")
                        .replaceAll(DIVID,"").replaceAll("\\.","")
                        .replaceAll("\\(","").replaceAll("\\)","");
                String lastSymbol = statementWithoutWhitespace.substring(statementWithoutWhitespace.length() - 1);
                if (!checkIfText.isEmpty() || lastSymbol.equals("\\+") || lastSymbol.equals(MINUS) ||
                        lastSymbol.equals("\\*") || lastSymbol.equals(DIVID) || lastSymbol.equals("\\("))
                {
                    return resultNull;
                } else
                { // this is a valid expression and begin to calculate
                    ArrayList<String> difficultExpression = new ArrayList<>();
                    ArrayList<String> statementList = getEachSymbol(statementWithoutWhitespace);
                    listFmInputedStatement = statementList;
                    inputedStatement = toStringFmArrayList(listFmInputedStatement);
//                    System.out.println("---63-------listFmInputedStatement->" + listFmInputedStatement);
                    if (statement.contains("(") || statement.contains(")"))
                    {
                        int openBrace = 0, closeBrace = 0;
                        for (String element : statementList) {
                            if (element.equals("(")) openBrace++;
                            if (element.equals(")")) closeBrace++;
                        }
                        if(openBrace != closeBrace) return resultNull;
                        else if (statementList.indexOf("(") > statementList.lastIndexOf(")")) return resultNull;
                        else
                        {      // if it is difficult expression with many parts of parentheses
                            int indexFirstOpenParenthes = 0;
                            int qtyOpenParenthes = 0;
                            int indexLastCloseParenthes = 0;
                            int qtyCloseParenthes = 0;
                            for (int i = 0; i < statementList.size(); i++)
                            {
                                if (statementList.get(i).equals("("))
                                {
                                    qtyOpenParenthes++;
                                    if (qtyOpenParenthes == 1) indexFirstOpenParenthes = i;
                                }
                                if (statementList.get(i).equals(")"))
                                {
                                    qtyCloseParenthes++;
                                    if (qtyCloseParenthes == qtyOpenParenthes)
                                    {
                                        String expression = "";
                                        indexLastCloseParenthes = i;
                                        for (int j = indexFirstOpenParenthes; j <= indexLastCloseParenthes; j++) {
                                            expression = expression + statementList.get(j);
                                        }
//                                        System.out.println("-------99--expression:" + expression);
                                        difficultExpression.add(expression);
                                        indexFirstOpenParenthes = 0;
                                        qtyOpenParenthes = 0;
                                        indexLastCloseParenthes = 0;
                                        qtyCloseParenthes = 0;
                                    }
                                }
                            }
//                            System.out.println("------ArrayList<String> difficultExpression->" + difficultExpression);
                            // if there are more than one expression in parentheses
                            if (difficultExpression.size() > 1)
                            {
                                ArrayList<String> withoutBracesList = new ArrayList<>();
                                ArrayList<String> bufferStatementList = new ArrayList<>();
                                String bufferStatement = "";
//                                System.out.println("----------118----difficultExpression.size():" + difficultExpression.size());
                                int stop = difficultExpression.size();
                                for (int r = 0; r < stop; r++)
                                {
                                    bufferStatementList = getEachSymbol(difficultExpression.get(r));
//                                    System.out.println("---difficultExpression-118--bufferStatementList->" + bufferStatementList);
                                    bufferStatement = difficultExpression.get(r);
//                                    System.out.println("---difficultExpression-120--bufferStatement->" + bufferStatement);
                                    String statementWithoutBraces = removeBraces(bufferStatementList, bufferStatement);
//                                    System.out.println("---difficultExpression-122--statementWithoutBraces->" + statementWithoutBraces);
                                    withoutBracesList.add(statementWithoutBraces);
//                                    System.out.println("---difficultExpression-124--withoutBracesList->" + withoutBracesList);
                                }
                                for (int i = 0; i < difficultExpression.size(); i++) {
//                                    System.out.println("--------133---inputedStatement:" + inputedStatement);
                                    inputedStatement = inputedStatement.replace(difficultExpression.get(i)
                                            ,withoutBracesList.get(i));
                                }
//                                System.out.println("--------137---inputedStatement:" + inputedStatement);
                                if (doMathematics(inputedStatement).contains("Infinity")) return resultNull;
                                else
                                {
                                    String resultString = doMathematics(inputedStatement);
                                    Double resultDouble = Double.parseDouble(resultString);
                                    BigDecimal resultBigDecimal = new BigDecimal(resultDouble).setScale(4, RoundingMode.CEILING);
                                    return getResultExpression(resultBigDecimal);
                                }
                            } else
                            {
                                String statementWithoutBraces = removeBraces(statementList, statement);
                                String resultString = doMathematics(statementWithoutBraces);
//                                System.out.println("*** 70 ****** statementWithoutBraces->" + statementWithoutBraces);
                                if (resultString.contains("Infinity")) return resultNull;
                                else
                                {
                                    Double resultDouble = Double.parseDouble(resultString);
                                    BigDecimal resultBigDecimal = new BigDecimal(resultDouble).setScale(4, RoundingMode.CEILING);
                                    return getResultExpression(resultBigDecimal);
                                }
                            }
                        }
                    } else
                    {
                        // no parentheses in expression
                        String statementWithoutBraces = toStringFmArrayList(statementList);
                        String resultString = doMathematics(statementWithoutBraces);
//                        System.out.println("---65-------statementWithoutBraces->" + statementWithoutBraces);
                        if (resultString.contains("Infinity")) return resultNull;
                        else
                        {
                            Double resultDouble = Double.parseDouble(resultString);
                            BigDecimal resultBigDecimal = new BigDecimal(resultDouble).setScale(4, RoundingMode.HALF_DOWN);
                            return getResultExpression(resultBigDecimal);
                        }
                    }
                }
            }
        }
    }

    public String getResultExpression(BigDecimal resultBigDecimal){
        String result = resultBigDecimal + "";
        if (result.contains("."))
        {
            String[] twoParts = result.split("\\.");
            if (twoParts[1].equals("0000")) result = twoParts[0];
            else if (twoParts[1].contains("0"))
            {
                String rightPart = twoParts[1];
                String buffer = "";
                for (int i = rightPart.length()-1; i >= 0; i--) {
                    if (i > 0 && rightPart.charAt(i) == '0')
                    {
                        buffer = rightPart.substring(0,i);
                        System.out.println("-----buffer:" + buffer);
                    } else if (i == 0 && rightPart.charAt(i) == '0')
                    {
                        result = twoParts[0];
                    } else {
                        result = twoParts[0] + "." + buffer;
                    }
                }
            }
        }
        return result;
    }

    @Override // make incoming expression correct by one standart phrase
    public ArrayList<String> getEachSymbol(String inputLine) {
        inputLine = inputLine.trim();
//        System.out.println("---84---getEachSymbol(String inputLine)->" + inputLine);
        ArrayList<String> statementList = new ArrayList<>();
        String element = "";
        for (int i = 0; i < inputLine.length(); i++) {
            String elementBuffer = inputLine.substring(i,i+1);
//            System.out.println("******* elementBuffer:" + elementBuffer);
            if (elementBuffer.equals("0") || elementBuffer.equals("1") || elementBuffer.equals("2") ||
                    elementBuffer.equals("3") || elementBuffer.equals("4") || elementBuffer.equals("5") ||
                    elementBuffer.equals("6") || elementBuffer.equals("7") || elementBuffer.equals("8") ||
                    elementBuffer.equals("9") || elementBuffer.equals(".")  || elementBuffer.equals(","))
            {
                element = element + elementBuffer;
                if (i+1 == inputLine.length())
                {
                    //System.out.println("the last:" + element);
                    statementList.add(element);
                }
            } else
            if (elementBuffer.equals("+") || elementBuffer.equals("-") || elementBuffer.equals("*") ||
                    elementBuffer.equals("/") || elementBuffer.equals("(") || elementBuffer.equals(")"))
            {
                if (!element.isEmpty())
                {
                    statementList.add(element);
                    statementList.add(" ");
                    element = "";
                }
                statementList.add(elementBuffer);
                statementList.add(" ");
            }
        }
//        System.out.println("---84---getEachSymbol statementList->" + statementList);
        return statementList;
    }

    @Override // incoming - correct expression with Parentheses, outgoing - expression without Parentheses for calculating
    public String removeBraces(ArrayList<String> statementList, String statement)
    {
        String result = "";
        String prepareRemoveBraces = "";
        while (statement.contains("(") || statement.contains(")"))
        {
//            System.out.println("---132-------removeBraces----begin");
            prepareRemoveBraces = prepareRemoveBraces(statementList, statement);
//            System.out.println("-----144 prepareRemoveBraces:" + prepareRemoveBraces);
            result = doMathematics(prepareRemoveBraces);
            if (result.equalsIgnoreCase("Infinity"))
            {
                statement = "Infinity";
                break;
            }
            Double d = Double.parseDouble(result);
//            System.out.println("-----145 Double d:" + d);
//            System.out.println("-----146 result:" + result);
            String statementToChange = "( " + prepareRemoveBraces.trim() + " )";
            statement = statement.replace(statementToChange, result);
//            System.out.println("-----154 statement.replace:" + statement);
            statementList = getEachSymbol(statement);
//            System.out.println("-----155 statementList:" + statementList);
            statement = toStringFmArrayList(statementList);
        }
//        System.out.println("--------------ffffff statement:" + statement);
        return statement;
    }

    public String prepareRemoveBraces(ArrayList<String> statementList, String statement)
    {
        String statementWithoutBraces = "";
//        System.out.println("----241--------statementList:" + statementList);
        while (statement.contains("(") || statement.contains(")"))
        {
            statementWithoutBraces = "";
            int stop = statementList.lastIndexOf(")");
            for (int i = 0; i < statementList.size(); i++)
            {
                if (statementList.get(i).contains("("))
                {
                    for (int j = i+1; j < stop; j++)
                    {
                        statementWithoutBraces += statementList.get(j);
//                        System.out.println("------137--statementWithoutBraces->" + statementWithoutBraces);
                    }
                    break;
                }
            }
            statementList = getEachSymbol(statementWithoutBraces);
//            System.out.println("-----aaa--statementList->" + statementList);
            statement = statementWithoutBraces;
//            System.out.println("-----bbb--statement->" + statement);
//            System.out.println("-----bbb111--statementWithoutBraces->" + statementWithoutBraces);
        }
        return statement;
    }

    // make string line expression from elements in List
    private String toStringFmArrayList(ArrayList<String> incomingArrayList)
    {
        String outputString = "";
        for (String newElement : incomingArrayList)
        {
            outputString = outputString + newElement;
        }
        return outputString;
    }

    @Override // make calculating
    public String doMathematics(String statementWithoutBraces)
    {
        ArrayList<String> calculStatementList = new ArrayList<>();
        for (String element : statementWithoutBraces.trim().split(" "))
        {
            calculStatementList.add(element);
        }
//        System.out.println("-------212--calculStatementList->" + calculStatementList + "-statementWithoutBraces->" + statementWithoutBraces);
        while (calculStatementList.size() >= 3) // != 0)
        {
            try
            {
                Double result = 0d;
                if (calculStatementList.indexOf("/") != -1)
                {
                    int index = calculStatementList.indexOf("/");
                    Double sign = 1d;
                    if (calculStatementList.get(index + 1).contains("-"))
                    {
                        calculStatementList.remove(index + 1);
                        sign = -1d;
                    }
                    result = Double.valueOf(calculStatementList.get(index - 1)) / Double.valueOf(calculStatementList.get(index + 1));
                    result *= sign;
//                System.out.println("****303******result:" + result);
                    calculStatementList.add(index - 1, String.valueOf(result));
                    calculStatementList.remove(index + 2);
                    calculStatementList.remove(index + 1);
                    calculStatementList.remove(index);
                }
                else if (calculStatementList.indexOf("*") != -1)
                {
//                System.out.println("****292******calculStatementList.size():" + calculStatementList.size());
                    int index = calculStatementList.indexOf("*");
                    Double sign = 1d;
//                System.out.println("****294******index:" + index);
                    if (calculStatementList.get(index + 1).contains("-"))
                    {
                        calculStatementList.remove(index + 1);
                        sign = -1d;
                    }
                    result = Double.valueOf(calculStatementList.get(index - 1)) * Double.valueOf(calculStatementList.get(index + 1));
                    result *= sign;
//                System.out.println("****303******result:" + result);
                    calculStatementList.add(index - 1, String.valueOf(result));
                    calculStatementList.remove(index + 2);
                    calculStatementList.remove(index + 1);
                    calculStatementList.remove(index);
                }
                else if (calculStatementList.indexOf("-") != -1)
                {
                    int index = calculStatementList.indexOf("-");
                    int lastIndex = calculStatementList.lastIndexOf("-");
//                System.out.println("----313---index==lastIndex:" + index + "==" + lastIndex);
                    if (index == 0)
                    {
                        result = 0.0 - Double.valueOf(calculStatementList.get(index + 1));
                        calculStatementList.add(0, String.valueOf(result));
                        calculStatementList.remove(2);
                        calculStatementList.remove(1);
                    }
                /*else if(calculStatementList.get(index + 1).contains("-"))
                {
                    calculStatementList.remove(index + 1);
                    calculStatementList.set(index,"+");
//                    System.out.println("----324---calculStatementList->" + calculStatementList);
                    result = Double.valueOf(calculStatementList.get(index - 1)) + Double.valueOf(calculStatementList.get(index + 1));
                    calculStatementList.set(index-1,String.valueOf(result));
                    calculStatementList.remove(index+1);
                    calculStatementList.remove(index);
//                    System.out.println("----329---calculStatementList->" + calculStatementList);
                }*/
                    else if ((lastIndex-2>0) && (calculStatementList.get(lastIndex-2).equals("-")))
                    {
                        result = Double.valueOf(calculStatementList.get(lastIndex + 1)) + Double.valueOf(calculStatementList.get(lastIndex - 1));
                        calculStatementList.add(lastIndex - 1, String.valueOf(result));
                        calculStatementList.remove(lastIndex + 2);
                        calculStatementList.remove(lastIndex + 1);
                        calculStatementList.remove(lastIndex);
                    }
                    else
                    {
                        if (calculStatementList.get(index - 1).contains("+"))
                        {
                            calculStatementList.remove(index - 1);
                            index--;
                        }
//                    System.out.println("---------338--calculStatementList->" + calculStatementList);
//                    System.out.println("---------339--index->" + index);
                        result = Double.valueOf(calculStatementList.get(index - 1)) - Double.valueOf(calculStatementList.get(index + 1));
//                    System.out.println("---------340--result->" + result);
                        calculStatementList.add(index - 1, String.valueOf(result));
                        calculStatementList.remove(index + 2);
                        calculStatementList.remove(index + 1);
                        calculStatementList.remove(index);
                    }
                }
                else if (calculStatementList.indexOf("+") != -1)
                {
                    int index = calculStatementList.indexOf("+");
//                System.out.println("-------348--calculStatementList->" + calculStatementList);
                    result = Double.valueOf(calculStatementList.get(index - 1)) + Double.valueOf(calculStatementList.get(index + 1));
                    calculStatementList.add(index - 1, String.valueOf(result));
                    calculStatementList.remove(index + 2);
                    calculStatementList.remove(index + 1);
                    calculStatementList.remove(index);
                }

                // check if missing (*/+-))
                if ((calculStatementList.indexOf("*") == -1) && (calculStatementList.indexOf("/") == -1) &&
                        (calculStatementList.indexOf("+") == -1) && (calculStatementList.indexOf("-") == -1))
                {
                    BigDecimal resultBigDecimal = new BigDecimal(result); //.setScale(4, RoundingMode.CEILING);
//                    System.out.println("---448---result:" + result);
//                    System.out.println("---449---resultBigDecimal:" + resultBigDecimal);
                    return resultBigDecimal + "";
                }
            } catch (IllegalArgumentException | IndexOutOfBoundsException e){
                //e.printStackTrace();
                return "Infinity";
            }
        }
        try { // insurance, if smthng strange with expression
//            System.out.println("-----366-----calculStatementList:" + calculStatementList);
            Double resultDouble = 0d; // = Double.valueOf(calculStatementList.get(0));
//            System.out.println("-----368-----calculStatementList.size():" + calculStatementList.size());
            if (calculStatementList.size() == 1)
            {
                resultDouble = Double.valueOf(calculStatementList.get(0));
            } else
            if (calculStatementList.size() > 1)
            {
//                System.out.println("+++++++++");
                if (statementWithoutBraces.contains("*") || statementWithoutBraces.contains("/"))
                {
                    resultDouble = 1.0 / 0;
//                    System.out.println("+++396++++++resultDouble:" + resultDouble);
                }
                else if (calculStatementList.get(0).contains("+"))
                    resultDouble = Double.valueOf(calculStatementList.get(1));
                else if (calculStatementList.get(0).contains("-"))
                    resultDouble = Double.valueOf(calculStatementList.get(0) + calculStatementList.get(1));
                else resultDouble = 1.0 / 0;
            }
            BigDecimal resultBigDecimal = new BigDecimal(resultDouble); //.setScale(4, RoundingMode.CEILING);
//            System.out.println("----457--resultDouble:" + resultDouble);
//            System.out.println("----458--resultBigDecimal:" + resultBigDecimal);
            return resultBigDecimal + "";
        } catch (IllegalArgumentException | IndexOutOfBoundsException e){
            //e.printStackTrace();
            return "Infinity";
        }
    }

}
